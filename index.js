require("dotenv").config();
const fs = require("fs");

const EXCHANGE_RATES = Number(process.env.EXCHANGE_RATES) || 70;
const SUM = Number(process.env.SUM) || 1;

const result = `${SUM}₽ = ${SUM/EXCHANGE_RATES}$\n${SUM}$ = ${SUM*EXCHANGE_RATES}₽`

fs.writeFileSync(
  "./output.json",
  JSON.stringify({
    typeof: typeof result,
    result,
  })
);
